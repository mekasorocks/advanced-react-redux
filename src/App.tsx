import {Button} from '@material-ui/core';
import * as React from 'react';
import './App.css';


class App extends React.Component {
  public render() {
    return (
      <Button variant="raised" color="primary" >KLICK ME</Button>
    );
  }
}

export default App;
